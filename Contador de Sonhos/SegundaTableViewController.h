//
//  SegundaTableViewController.h
//  Contador de Sonhos
//
//  Created by Samuel Lucas Ribeiro Cortez on 25/03/15.
//  Copyright (c) 2015 br.pucpr.bepid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SegundaTableViewController : UITableViewController <UISearchBarDelegate, UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UITableView *myTableView;
    IBOutlet UISearchBar *mySearchbar;
    NSMutableArray *totalstring;
    NSMutableArray *filteredString;
    BOOL isFilltered;
}

@property (retain, nonatomic) NSString * contoSelecionado;

@property (retain, atomic) NSMutableArray * conteudoArquivos;

@property (retain, atomic) NSArray * dataCompleta;

@property (retain) NSMutableArray * contos;

@property (retain) NSMutableArray *conteudoPorSecao;


@end
