//
//  CriarViewController.m
//  Contador de Sonhos
//
//  Created by Samuel Lucas Ribeiro Cortez on 26/03/15.
//  Copyright (c) 2015 br.pucpr.bepid. All rights reserved.
//

#import "CriarViewController.h"
#import "FilesManager.h"

@implementation CriarViewController{
    NSMutableArray *dataCell;
    NSString *dirConto;
    NSString *dirDataCell;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)salvarConto:(id)sender {
    if ((_meuTitulo.text.length > 0) || (_meuConto.text.length > 0)) {
        
        NSString *lerValores = [FilesManager LoadStringFromFile:@"" useBundle:YES];
        dataCell = [[lerValores componentsSeparatedByString:@";"] mutableCopy];
        [dataCell insertObject:_meuTitulo.text atIndex:0];
        
        
        
        
        
        
        NSString *caminhoDocs = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
        dirConto = [caminhoDocs stringByAppendingString:@"/"];
        dirDataCell = [dirConto stringByAppendingString:@"DataCells.csv"];
        dirConto = [dirConto stringByAppendingString:(@"%@", _meuTitulo.text)];
        dirConto = [dirConto stringByAppendingString:@".csv"];
        
        
        
        
        
        [[NSFileManager defaultManager] createFileAtPath:dirConto
                                                contents:nil
                                              attributes:nil];
        
        [[NSFileManager defaultManager] createFileAtPath:dirDataCell
                                                contents:nil
                                              attributes:nil];
        
        
        
        NSFileHandle * manageArchiveConto = [NSFileHandle fileHandleForUpdatingAtPath:dirConto];
        NSFileHandle * manageArchiveDataCell = [NSFileHandle fileHandleForUpdatingAtPath:dirDataCell];
        
        
        
        [manageArchiveConto seekToEndOfFile];
        [manageArchiveConto writeData:[_meuConto.text dataUsingEncoding: NSUTF32StringEncoding]];
        [manageArchiveConto closeFile];
        
        
        
        NSString *arrayToString = [dataCell componentsJoinedByString:@";"];
        [manageArchiveDataCell seekToEndOfFile];
        [manageArchiveDataCell writeData:[arrayToString dataUsingEncoding: NSUTF32StringEncoding]];
        [manageArchiveDataCell closeFile];


    }else{
        
        UIAlertView *alertaFalha = [[UIAlertView alloc] initWithTitle:@"Fail to save"
                                                              message:@"Either title or story is empty"
                                                             delegate:self
                                                    cancelButtonTitle:@"Review data"
                                                    otherButtonTitles: nil];
        [alertaFalha show];
    }
    [self performSegueWithIdentifier:@"criado" sender:self];
}

@end