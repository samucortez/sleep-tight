In the happy days of Long Ago, when every child had a Fairy Godmother, and every Fairy Godmother had a Magic Wand, there lived a little girl whose name was Ella. She was a very happy little girl as long as her mother was alive, but when her dear mother died all was changed. For, very soon, her father brought home a new wife who did not like little girls at all, and only cared for her own two proud, ugly daughters.
Now these two daughters were so idle and selfish that they made their poor little step-sister do all the work. She washed the dishes and cooked the dinner, she swept the rooms and made the beds, and when all the housework was finished she was obliged to wait on her two proud sisters, make their fine dresses, and brush their hair when they went to bed. And at last, when everything was done, she would be so tired that [106] she could only creep near the kitchen fire and rest among the cinders.
"Sitting among the cinders as usual!" her stepmother would cry. "You ought to be called Cinder-Ella instead of Ella."
And soon no one thought of calling her anything but Cinderella.
It happened about this time that the Prince, whose father was King of that country, was nearly twenty-one years old, and to celebrate his birthday a splendid ball was to be given at the royal palace. Invitations were sent out far and near, and great was the delight of Cinderella's step-sisters when they also received an invitation, sealed with the royal seal. They were so wild with joy that they could not help calling Cinderella from the kitchen to hear the good news.
"I suppose it is not meant for me as well?" asked Cinderella wistfully.
"You indeed!" screamed the elder sister. "Who would ask a dusty little ash-sweeper to go to a royal ball?"
"Go back to your work," said the second [107] sister with a scornful toss of her head. "You go to a ball indeed!"
So Cinderella went sadly back to her work, and all the time she was sweeping and dusting she dreamed of the lovely ball and the beautiful dresses, and at night she cried herself to sleep because she could not go.
What a stir there was when the night of the ball arrived. Poor Cinderella needed a dozen pairs of hands to do all that she was told to do.
"Come and brush my hair," cried the eldest sister.
"Be quick and fasten my shoes," said the second.
"And fetch my fan."
"And find my gloves."
But at last they were all dressed, and they stepped into the carriage and drove off to the ball, telling Cinderella to take care of the house, and to sit up to let them in. Poor Cinderella! It really was more than she could bear, and she sat down by the kitchen fire and sobbed as if her heart would break. She wanted to go to the ball [108] so much, and she was so tired of work. She did not even care to knit, and she sat sadly gazing into the fire while the black kitten rolled her ball of wool into a dreadful tangle.

[Illustration]

She sat sadly gazing into the fire

"What are you crying about, my dear?" cried a shrill old voice at her elbow, and Cinderella started with surprise.
She had thought she was all alone, but there by the window, through which the new moon was peeping, stood a little old lady. She was dressed in a red cloak, and a queer black-pointed hat, and in her hand she held a Magic Wand.
"What are you crying about, my dear?" she asked again. And she smiled so kindly that poor Cinderella felt sure she was a friend.
"I am crying because I cannot go to the ball," answered Cinderella, getting up to drop a curtsey. "Please, may I ask who you are?"
"Do you not know me, child?" asked the old woman. "I am your Fairy Godmother, and I have come to-night to give you your wish."
[109] "Oh, Godmother dear," cried Cinderella, her eyes shining with joy. "Do you really mean that I shall go to the ball? But how can I go in this dusty old frock?" And her face grew sad again.
"Never mind the frock," said her Godmother. "Do exactly as I tell you, and leave the rest to me. First, go into the garden and bring me the biggest pumpkin you can find."
Cinderella ran to the garden as quickly as she could, and brought back a great yellow pumpkin and put it just outside the door.
"Now look if there are any mice in the mouse-trap, and bring it to me," said the Fairy Godmother.
Cinderella ran to the cupboard and brought the mouse-trap, with six mice in it.
"Now fetch the rat-trap," ordered her Godmother.
And Cinderella brought the rat-trap from the cellar, with a fat old rat in it.
"That is well," said the Fairy Godmother, nodding her head. "There is just one more thing I want. Look on the warmest side of [110] the garden wall, and bring me two green lizards that you will find there."
"Here they are, Godmother," said Cinderella as she placed them beside the pumpkin, and the mouse-trap, and the rat-trap.
Then the Fairy Godmother waved her Magic Wand once over the pumpkin, and in the twinkling of an eye it was changed into a golden coach! Another wave of the wand, and the mice were changed into six cream-coloured ponies, the rat into a fat old coachman, and the two lizards into footmen in smart green liveries.
Cinderella clapped her hands with delight, but the Fairy Godmother had not finished yet. She waved her Magic Wand once more, and the old ragged frock which Cinderella wore was changed into the most beautiful robe of white shining gauze, which looked as if it had been woven from moonbeams and spangled with silver stars.
"O Godmother! Godmother!" cried Cinderella, "how can I ever thank you?"
"Never mind about thanking me," said her Godmother, "but remember to do exactly as [111] I bid you. You must leave the ball before twelve o'clock, for when that hour strikes, the magic spell will lose its power. The coach will be a pumpkin once more, the ponies will be turned into mice, the coachman will be a rat again, the footmen will be lizards, and your beautiful dress will vanish and only your old rags will be left behind."
"Indeed, I will do as you tell me," said Cinderella earnestly. "But, Godmother, how am I to dance in these old shoes?" And she pointed down to the worn-out shoes which peeped from beneath her beautiful dress.
"Tut, tut, I forgot the shoes," said her Godmother. And then she took out of her pocket the most exquisite little pair of glass-slippers, which fitted as if they had been made for her.
"Now away to the ball, and do not forget what I have told you," said the Fairy Godmother.
So Cinderella stepped into the golden coach, the coachman cracked his whip, the [112] footmen jumped up behind, and the six cream-coloured ponies went off like the wind.
The dancing had just begun at the palace, and all the fair ladies were wondering who would be asked to open the ball with the Prince, when a beautiful stranger entered the ball-room. The Prince held his breath with surprise; he had never seen any one half so beautiful before. Every one turned to look at her, and Cinderella's proud sisters whispered to each other, "What a lovely dress! She must be a Princess." They, of course, never dreamed that it could be their step-sister Cinderella, whom they had left alone by the kitchen fire at home.
All night long the Prince would dance with no one but the beautiful stranger, and the time passed so quickly, and Cinderella was so happy, that she quite forgot to look at the clock. But just as the Prince had led her out into the balcony, and wished to know her name, the clock began to strike twelve!
In a flash Cinderella remembered her Godmother's warning, and she darted away [113] and ran down the steps as fast as her feet could carry her. But in her haste she tripped, and one of her glass-slippers came off. She could not stop to pick it up, for the Prince was following close behind, and so she ran on without it. Just as she reached the great door the last stroke of twelve sounded, and when the Prince came running up and asked the guards if any one had passed that way, they said, "Only a poor girl in rags, your Highness."
Then the Prince went sadly back, for all that was left of the lovely stranger was the little shining glass-slipper which he had picked up.
Now what the Fairy Godmother had said was quite true, for when the last stroke of twelve sounded all the magic was undone. The golden coach and cream-coloured ponies, the coachman and footmen disappeared, and only a pumpkin stood in the courtyard, and six little mice scampered away, followed by a fat old rat and two green lizards. Cinderella's beautiful dress vanished, and she had only on her old patched frock as she [114] ran all the way home. The only thing left was the little glass-slipper, which she hid carefully away as soon as she reached home. Then she sat down by the fire to wait for the return of her stepmother and her two proud sisters.
"Sitting among the cinders as usual," they cried when they came in. "Come, be quick and help us get ready for bed."
"Please tell me about the ball," said Cinderella humbly.
"What should you know about balls?" said the eldest sister crossly. She was in a very bad temper because the Prince had never asked her to dance with him.
"Were there many beautiful ladies there?" asked Cinderella.
"None as beautiful as we were," said the second sister, "except perhaps a strange Princess who danced all night with the Prince."
"Do you think I shall ever go to a ball? asked Cinderella as she brushed their hair.
"A pretty figure you would be at a ball!" they both cried out together. "Be off to [115] bed; you must be dreaming to think of such a thing."
Meanwhile the Prince could think of nothing else but the beautiful lady whose name even he did not know, and next day he sent round heralds throughout the kingdom with a royal proclamation.
"To each and every subject in our kingdom be it known, whereas last night a glass-slipper was found in the royal palace, whomsoever it shall fit, she alone shall be the Prince's Bride."
And the glass-slipper was placed on a purple velvet cushion and carried around to every house, that each lady might try it on.
When the herald arrived at the house where Cinderella lived, the proud sisters were so excited that their hands quite trembled.
"I shall try it on first," said the eldest.
"My foot is smaller than yours," said the second, and they pushed each other very rudely.
But the eldest snatched up the slipper first, and began to try it on. She pushed [116] and tugged till her face grew quite scarlet, but the shoe would not go even half-way on.
Then the second sister took it and managed to squeeze her toes in, but the heel would not go on.
"Silly thing; I don't believe it was meant to be worn," she cried, as she kicked it off and burst into tears.
Meanwhile Cinderella had crept quietly into the room, and as the herald picked up the shoe, she said in a low voice, "Please may I try on the glass-slipper?"
"How dare you come in here?" cried the eldest sister.
"You try on the shoe indeed!" cried the second one. "Be off to the kitchen and sit among your cinders."
But the herald bowed low to Cinderella and offered her the slipper upon the velvet cushion.
"It is the Prince's command that every one shall try on the slipper," he said.
Then Cinderella sat down and fitted on the little glass-slipper as easily as if it had been made for her, and while every [117] one stared with surprise, she took from her pocket the other slipper which matched it.
"It certainly fits her," said the herald.
"Take it off at once," screamed the two sisters.
But before they could snatch off the shoe, a little old woman stood in front of Cinderella and waved her Magic Wand. And in an instant the old ragged dress had vanished, and there stood the beautiful lady of the ball, in her dress of woven moonbeams spangled with silver stars.
The Prince did not wait to ask if the glass-slipper had fitted her, for he knew his beautiful lady at once, and the wedding-bells were rung that very day.
The two proud sisters were sorry now that they had been so unkind to Cinderella, but she quite forgot all they had made her suffer, and was as kind to them as if they had been her own sisters.
And so Cinderella married the Prince, and lived happily ever after. She went to all the balls and always wore the glass- [118] slippers, for they never grew old, but always looked as shining and beautiful as on that first night when the Fairy Godmother had brought them out of her magic pocket.