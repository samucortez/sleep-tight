//
//  FilesManager.h
//  Contador de Sonhos
//
//  Created by Samuel Lucas Ribeiro Cortez on 25/03/15.
//  Copyright (c) 2015 br.pucpr.bepid. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FilesManager : NSFileManager

+(NSString *)LoadStringFromFile:(NSString *)file useBundle:(BOOL)bundle;


@end
