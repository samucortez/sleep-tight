//
//  SegundaTableViewController.m
//  Contador de Sonhos
//
//  Created by Samuel Lucas Ribeiro Cortez on 25/03/15.
//  Copyright (c) 2015 br.pucpr.bepid. All rights reserved.
//

#import "SegundaTableViewController.h"
#import "SegundaTableViewCell.h"
#import "FilesManager.h"
#import "HistoriaViewController.h"

@implementation SegundaTableViewController{
    NSString * listaTudo;
    NSFileHandle * manageArchive;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    mySearchbar.delegate = self;
    
    
    
    
    NSString *caminhoDocs = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
    listaTudo = [caminhoDocs stringByAppendingString:@"/DataCells.csv"];
    
    
    
    
    
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:listaTudo]){
        [[NSFileManager defaultManager] createFileAtPath:listaTudo
                                                contents:nil
                                              attributes:nil];
        
        manageArchive = [NSFileHandle fileHandleForUpdatingAtPath:listaTudo];
        [manageArchive seekToEndOfFile];
        [manageArchive writeData:[[NSString stringWithFormat:
                                    @"Beauty and the Beast;Peter Pan;Rapunzel;Cinderella"]
                                   dataUsingEncoding:NSUTF32StringEncoding]];
        [manageArchive closeFile];
    }
    
    
    
    
    
    NSString *lerValores = [FilesManager LoadStringFromFile:@"" useBundle:YES];
    _conteudoArquivos = [[lerValores componentsSeparatedByString:@";"] mutableCopy];
    _contos = [@[@"A",
                 @"B", @"C",
                 @"D", @"E", @"F",
                 @"G", @"H", @"I", @"J",
                 @"K", @"L", @"M", @"N", @"O",
                 @"P", @"Q", @"R", @"S", @"T", @"U",
                 @"V", @"W", @"X", @"Y", @"Z"] mutableCopy];
    
    NSString * carregarDados = [FilesManager LoadStringFromFile:@"" useBundle:YES];
    lerValores = [carregarDados stringByReplacingOccurrencesOfString:@"$" withString:@";"];

    
    
    
    _conteudoArquivos = [[lerValores componentsSeparatedByString:@";"] mutableCopy];
    _conteudoArquivos = [[_conteudoArquivos sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] mutableCopy];
    
    
    
    
    _conteudoPorSecao =
    @[[self PrimeiraLetra:@"A"],[self PrimeiraLetra:@"B"],[self PrimeiraLetra:@"C"],[self PrimeiraLetra:@"D"],[self PrimeiraLetra:@"E"],[self PrimeiraLetra:@"F"],[self PrimeiraLetra:@"G"],[self PrimeiraLetra:@"H"],[self PrimeiraLetra:@"I"],[self PrimeiraLetra:@"J"],[self PrimeiraLetra:@"K"],[self PrimeiraLetra:@"L"],[self PrimeiraLetra:@"M"],[self PrimeiraLetra:@"N"],[self PrimeiraLetra:@"O"],[self PrimeiraLetra:@"P"],[self PrimeiraLetra:@"Q"],[self PrimeiraLetra:@"R"],[self PrimeiraLetra:@"S"],[self PrimeiraLetra:@"T"],[self PrimeiraLetra:@"U"],[self PrimeiraLetra:@"V"],[self PrimeiraLetra:@"X"],[self PrimeiraLetra:@"Y"],[self PrimeiraLetra:@"Z"],[self PrimeiraLetra:@"0"]];
    
    
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    [myTableView reloadData];

}



- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [myTableView resignFirstResponder];
}






- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if (searchText.length == 0){
        
        
        
        isFilltered = NO;
        
        
    }else{
        isFilltered = YES;
        filteredString = [[NSMutableArray alloc] init];
        
        
        
        for(NSString *str in _conteudoArquivos){
            NSRange stringRange = [str rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if (stringRange.location != NSNotFound) {
                [filteredString addObject:str];
            }
        }
    }
    
    
    [myTableView reloadData];
}



- (NSMutableArray *)PrimeiraLetra:(NSString *)letra;{
    
    NSMutableArray * stringsEncontradas = [[NSMutableArray alloc] init];
    for (NSString *obj in _conteudoArquivos)
        {
        if ([[[obj substringToIndex:1] uppercaseString] isEqualToString:[letra uppercaseString]]){
            [stringsEncontradas addObject:obj];
        }
    
    }
    
    
    if (stringsEncontradas == nil) {
        stringsEncontradas = [@[@"empty"] mutableCopy];
    }
    
    
    return stringsEncontradas;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (!isFilltered) {
        return [_contos count];
    }else{
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (!isFilltered) {
        return [_conteudoPorSecao[section] count];
    }else{
        return [filteredString count];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SegundaTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"celularAlfabetica" forIndexPath:indexPath];
    static NSString *cellIdentifier = @"cell";
    if(!cell){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    if (!isFilltered) {
        cell.textLabel.text = _conteudoPorSecao[indexPath.section][indexPath.row];
    }else{
        cell.textLabel.text = [filteredString objectAtIndex:indexPath.row];
    }
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (!isFilltered) {
        _contoSelecionado = _conteudoPorSecao[indexPath.section][indexPath.row];
    }else{
        _contoSelecionado = [filteredString objectAtIndex:indexPath.row];
    }
    [self performSegueWithIdentifier:@"resultado" sender:self];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (!isFilltered) {
        return _contos[section];
    }else{
        return @"Resultados";
    }
}


- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    return _contos;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue identifier] isEqualToString:@"resultado"]){
        
        HistoriaViewController *resultado = [segue destinationViewController];
        resultado.contoEscolhido = _contoSelecionado;

    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        NSString * nomeExcluir = _conteudoPorSecao[indexPath.section][indexPath.row];
        [self.conteudoArquivos removeObjectAtIndex:indexPath.row];
        
        
        NSString *caminhoDocs = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
        NSString * dirConto = [caminhoDocs stringByAppendingString:@"/"];
        NSString * dirDataCell = [dirConto stringByAppendingString:@"DataCells.csv"];
        NSString *lerValores = [FilesManager LoadStringFromFile:@"" useBundle:YES];
        NSMutableArray *dataCell = [[lerValores componentsSeparatedByString:@";"] mutableCopy];
        [[NSFileManager defaultManager] createFileAtPath:dirDataCell
                                                contents:nil
                                              attributes:nil];
        [dataCell removeObject:(@"%@", nomeExcluir)];
        NSString *arrayToString = [dataCell componentsJoinedByString:@";"];
        NSFileHandle * manageArchiveDataCell = [NSFileHandle fileHandleForUpdatingAtPath:dirDataCell];
        [manageArchiveDataCell seekToEndOfFile];
        [manageArchiveDataCell writeData:[arrayToString dataUsingEncoding: NSUTF32StringEncoding]];
        [manageArchiveDataCell closeFile];
    }
    [tableView reloadData];
    [self viewDidLoad];

}
- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
@end