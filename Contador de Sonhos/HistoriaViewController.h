//
//  HistoriaViewController.h
//  Contador de Sonhos
//
//  Created by Samuel Lucas Ribeiro Cortez on 26/03/15.
//  Copyright (c) 2015 br.pucpr.bepid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoriaViewController : UIViewController

@property NSString *contoEscolhido;

@property (weak, nonatomic) IBOutlet UINavigationItem *meuTitulo;
@property (weak, nonatomic) IBOutlet UITextView *meuTexto;
@end
