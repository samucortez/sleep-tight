//
//  HistoriaViewController.m
//  Contador de Sonhos
//
//  Created by Samuel Lucas Ribeiro Cortez on 26/03/15.
//  Copyright (c) 2015 br.pucpr.bepid. All rights reserved.
//

#import "HistoriaViewController.h"

@implementation HistoriaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"Historia: %@", _contoEscolhido);
    NSString *caminhoDocs = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
    caminhoDocs = [caminhoDocs stringByAppendingString:@"/"];
    caminhoDocs = [caminhoDocs stringByAppendingString:(@"%@",_contoEscolhido)];
    caminhoDocs = [caminhoDocs stringByAppendingString:@".csv"];
    
    
    
    NSFileHandle * manageArchiveConto = [NSFileHandle fileHandleForReadingAtPath:caminhoDocs];
    NSString *conteudoConto = [[NSString alloc] initWithData:[manageArchiveConto availableData] encoding:NSUTF32StringEncoding];
    
    //NSString *conteudoConto = [[NSString alloc] initWithData:[manageArchiveConto availableData] encoding:NSUTF32StringEncoding];
    _meuTitulo.title = _contoEscolhido;
    _meuTexto.text = conteudoConto;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
