//
//  FilesManager.m
//  Contador de Sonhos
//
//  Created by Samuel Lucas Ribeiro Cortez on 25/03/15.
//  Copyright (c) 2015 br.pucpr.bepid. All rights reserved.
//

#import "FilesManager.h"

@implementation FilesManager


+(NSString *)LoadStringFromFile:(NSString *)file useBundle:(BOOL)bundle{
    
    
    NSString *documentPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
    documentPath = [documentPath stringByAppendingString:@"/DataCells.csv"];
    
    NSFileHandle * lerDados = [NSFileHandle fileHandleForUpdatingAtPath:documentPath];
    NSString * conteudoArquivo = [[NSString alloc] initWithData:[lerDados availableData] encoding:NSUTF32StringEncoding];


    return conteudoArquivo;
}

@end
