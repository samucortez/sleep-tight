//
//  SegundaTableViewCell.h
//  Contador de Sonhos
//
//  Created by Samuel Lucas Ribeiro Cortez on 25/03/15.
//  Copyright (c) 2015 br.pucpr.bepid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SegundaTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelSegunda;

@end
