//
//  CriarViewController.h
//  Contador de Sonhos
//
//  Created by Samuel Lucas Ribeiro Cortez on 26/03/15.
//  Copyright (c) 2015 br.pucpr.bepid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CriarViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *meuTitulo;
@property (weak, nonatomic) IBOutlet UITextView *meuConto;

- (IBAction)salvarConto:(id)sender;
@end
